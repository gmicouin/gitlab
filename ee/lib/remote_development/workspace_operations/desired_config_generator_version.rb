# frozen_string_literal: true

module RemoteDevelopment
  module WorkspaceOperations
    module DesiredConfigGeneratorVersion
      VERSION_1 = 1
      VERSION_2 = 2
      VERSION_3 = 3
      LATEST_VERSION = VERSION_3
    end
  end
end
