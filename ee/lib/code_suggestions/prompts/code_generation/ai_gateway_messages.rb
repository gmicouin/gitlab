# frozen_string_literal: true

module CodeSuggestions
  module Prompts
    module CodeGeneration
      class AiGatewayMessages < CodeSuggestions::Prompts::Base
        include Gitlab::Utils::StrongMemoize

        PROMPT_COMPONENT_TYPE = 'code_editor_generation'
        PROMPT_ID = 'code_suggestions/generations'

        # response time grows with prompt size, so we don't use upper limit size of prompt window
        MAX_INPUT_CHARS = 50000
        GATEWAY_PROMPT_VERSION = 3

        def request_params
          {
            prompt_components: [
              {
                type: PROMPT_COMPONENT_TYPE,
                payload: {
                  file_name: file_name,
                  content_above_cursor: content_above_cursor,
                  content_below_cursor: content_below_cursor,
                  language_identifier: language.name,
                  prompt_id: PROMPT_ID,
                  stream: params.fetch(:stream, false),
                  prompt_enhancer: code_generation_enhancer
                }
              }
            ]
          }
        end

        private

        def code_generation_enhancer
          # the fields here are used in AIGW to populate prompt template
          # updating the key names here will break the template rendering
          # please reach out to @code-creation-team in case of updating the hash
          {
            **examples_section_params,
            **existing_code_block_params,
            **context_block_params,
            **libraries_block_params,
            **user_instruction_params
          }
        end

        def examples_section_params
          {
            # TODO: we can migrate all examples to AIGW as followup,
            # eg: CODE_GENERATIONS_EXAMPLES_URI = 'ee/lib/code_suggestions/prompts/code_generation/examples.yml'
            examples_array: language.generation_examples(type: params[:instruction]&.trigger_type)
          }
        end

        def existing_code_block_params
          trimmed_content_above_cursor = content_above_cursor.to_s.last(MAX_INPUT_CHARS)
          trimmed_content_below_cursor = content_below_cursor.to_s.first(MAX_INPUT_CHARS -
           trimmed_content_above_cursor.size)

          {
            trimmed_content_above_cursor: trimmed_content_above_cursor,
            trimmed_content_below_cursor: trimmed_content_below_cursor
          }
        end

        def context_block_params
          related_files = []
          related_snippets = []

          params[:context]&.each do |context|
            if context[:type] == ::Ai::AdditionalContext::CODE_SUGGESTIONS_CONTEXT_TYPES[:file]
              related_files << <<~FILE_CONTENT
              <file_content file_name="#{context[:name]}">
              #{context[:content]}
              </file_content>
              FILE_CONTENT
            elsif context[:type] == ::Ai::AdditionalContext::CODE_SUGGESTIONS_CONTEXT_TYPES[:snippet]
              related_snippets << <<~SNIPPET_CONTENT
              <snippet_content name="#{context[:name]}">
              #{context[:content]}
              </snippet_content>
              SNIPPET_CONTENT
            end
          end

          {
            related_files: related_files,
            related_snippets: related_snippets
          }
        end

        def libraries_block_params
          if libraries.any?
            Gitlab::InternalEvents.track_event(
              'include_repository_xray_data_into_code_generation_prompt',
              project: params[:project],
              namespace: params[:project]&.namespace,
              user: params[:current_user]
            )
          end

          { libraries: libraries }
        end

        def libraries
          return [] unless xray_report

          xray_report.libs.map { |l| l['name'] }.first(MAX_LIBRARIES) # rubocop:disable Rails/Pluck -- libs is an array
        end
        strong_memoize_attr :libraries

        def xray_report
          ::Projects::XrayReport.for_project(params[:project]).for_lang(language.x_ray_lang).first
        end
        strong_memoize_attr :xray_report

        def user_instruction_params
          instruction = params[:instruction]&.instruction.presence ||
            'Generate the best possible code based on instructions.'

          { user_instruction: instruction }
        end
      end
    end
  end
end
