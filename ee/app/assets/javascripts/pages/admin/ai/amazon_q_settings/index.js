import SettingsApp from 'ee/amazon_q_settings/components/app.vue';
import { initSimpleApp } from '~/helpers/init_simple_app_helper';

initSimpleApp('#js-amazon-q-settings', SettingsApp);
